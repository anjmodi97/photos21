package controller;

import java.io.IOException;
import model.*;
import java.util.List;
import java.util.Optional;

import application.Main;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import model.UserList;




public class LoginController {
	
	@FXML
	private Button submit;
	
	 @FXML
	private TextField user;
	
	//@FXML
	//private ObservableList<String> users = null;
	
	 
	
	//public void setMainStage(Stage mstage) {
		// TODO Auto-generated method stub
		
	//}
	
	public void onButton(ActionEvent e) throws ClassNotFoundException, IOException {
		
		if(e.getSource()== submit) {
			checkuser(e);
			
		}
			
		return;
		}
		
		
	public void checkuser(ActionEvent event) throws ClassNotFoundException, IOException {
			
			UserList ulist= new UserList();
			
				try {
					ulist = UserList.read();
				} catch (ClassNotFoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				//System.out.println("list in login"+ ulist);
			
			Parent parent = null;
			
			String a = user.getText();
				
				if(a.contentEquals("admin")==true) { //admin user 
					
					//System.out.println("logging into admin");
					FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/AdminSub.fxml"));
					try {
						parent = (Parent) loader.load();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					        
					AdminController controller = loader.getController();
					Scene scene = new Scene(parent);
								
					Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();	
				                
					controller.start(app_stage);
				             
				    app_stage.setScene(scene);
				    app_stage.show();  
				    
				    return;
				}
				
			
				else if(ulist.userExists(a)) { //if song with title and artist already exist in library
					
					FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/NonAdminSub.fxml"));
					try {
						parent = (Parent)loader.load();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					        
					AlbumController controller = loader.<AlbumController>getController();
					
					controller.setUlist(ulist);
					controller.setUser(ulist.getUserByUsername(a));
					
					Scene scene = new Scene(parent);
								
					Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();	
				                
					controller.start(app_stage);
				             
				    app_stage.setScene(scene);
				    app_stage.show();  
					
				}
				else{
					Alert err = new Alert(AlertType.ERROR);
					err.setContentText("User not found. Try Again");
					err.showAndWait();
					}
				
				return;
		}

}
