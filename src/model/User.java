package model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import model.Album;



public class User implements Serializable {

	private String username;
	private List<Album> album;

	
	public User(String username) {
		
		this.username = username;
		album = new ArrayList<Album>();

	}
	
	
	public String getUsername() {
		return username;
	}
	
	public List<Album> getAlbums() {
		return album;
	}
	
	public void addAlbum(Album a) {
		album.add(a);
	}
	
	public boolean albumexists(String name) {
		for (Album a: album)
			if (a.getAlbumName().toLowerCase().equals(name.toLowerCase()))
				return true;
		
		return false;
		
	}
	
	public Album getThisAlbum(String name) {
		for (Album a: album) {
			if (a.getAlbumName().toLowerCase().equals(name.toLowerCase())) {
				return a;
			}
		}
		
		return null;
		
	}
	
	public void removeAlbum(Album a)
	{
		album.remove(a);
	}
	
	@Override
	   public String toString() {
	      return "Username: " + username;
	   }
}

	