package model;

import java.io.Serializable;
import java.util.*;
import javafx.scene.image.*;
/**
 * Class for image that implements serializable 
 * @author Administration
 *
 */
public class SImage implements Serializable{
	private int width;
	private int height;
	private int[][] pixel;
	
	/**
	 * default constructor
	 */
	public SImage(){
		
	}
	
	public void setImage(Image img){
		int w;
		int h;
		width = ((int)img.getWidth());
		height = ((int)img.getHeight());
		pixel = new int[width][height];
		
		PixelReader read = img.getPixelReader();
		for(w = 0; w < width; w++) {
			for(h = 0; h < height; h++) {
				pixel[w][h] = read.getArgb(w, h);
			}
		}
	}
	
	public Image getImage() {
		int w;
		int h;
		WritableImage img = new WritableImage(width, height);
		PixelWriter write = img.getPixelWriter();
		
		for(w = 0; w < width; w++) {
			for(h = 0; h < height; h++) {
				write.setArgb(w, h, pixel[w][h]);
			}
		}
		return img;
		
	}
	
	public int[][] getPixel(){
		return pixel;
	}
	
	public int getHeight() {
		return height;
	}
	
	public int getWidth() {
		return width;
	}
	
	
	public boolean equals(SImage img) {
		int w;
		int h;
		if(width != img.getWidth()) {
			return false;
		}
		if(height != img.getHeight()) {
			return false;
		}
		for(w = 0; w < width; w++) {
			for(h = 0; h < height; h++) {
				if(pixel[w][h] != img.getPixel()[w][h]) {
					return false;
				}
			}
		}
		return true;
	}
}