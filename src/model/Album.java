package model;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.*;



/**
 * model class of Album
 * class implements Serializable
 * 
 * Class contains a string for Album name, a list of photos, most recently added photo, and how many photos are in the album
 * 
 * @author Clara Le
 * @author Anjali Modi
 *
 */
public class Album implements Serializable{
	private String albumName;
	private List<Photo> photos;
	private Photo newestPhoto;
	private int photoCount;
	private String olddate;
	private String newdate;
	
	/**
	 * Constructor for the album class
	 * @param albumName = a String. the album name that the user inputs
	 * 
	 */
	public Album(String albumName) {
		this.albumName = albumName;
		photos = new ArrayList<Photo>();
		newestPhoto = null;
		photoCount = 0;
		olddate = "";
		newdate = "";
		
	}
	/**
	 * Method to set album name
	 * @param albumN = name of the album
	 */
	public void setAlbumName(String albumN) {
		this.albumName = albumN;
	}
	/**
	 * method to get album name
	 * @return String album name
	 */
	public String getAlbumName() {
		return albumName;
	}
	
	/**
	 * method to add a new photo to album
	 * @param newPhoto = the photo to be added
	 */
	public void addPhoto(Photo newPhoto) {
		photos.add(newPhoto);
		this.newestPhoto = newPhoto;
		this.photoCount = photoCount + 1;
		setOldestDate();
		setNewestDate();
	}
	
	/**
	 * method to delete a photo from the album
	 * @param deleteP = the photo to be deleted
	 */
	public void deletePhoto(Photo deleteP) {
		photos.remove(deleteP);
		if(deleteP.equals(newestPhoto)) {
			this.newestPhoto = getNewest();
		}
		this.photoCount = photoCount - 1;
		setOldestDate();
		setNewestDate();
	}
	
	/**
	 * method to get the most recently added photo, only used if deleting old newest photo
	 * @return the most recently added photo
	 */
	
	public Photo getOldest() {
		return photos.get(0);
	}
	
	public Photo getNewest() {
		if(photos.size() == 0) {
			return null;
		}
		
		else {
			return photos.get(photos.size() - 1);
		}
	}
	/**
	 * method to return how many photos are in the album
	 * @return how many photos are in the album.
	 */
	public int getSize() {
		return photoCount;
	}
	
	public List<Photo> getPhotos(){
		return photos;
	}
	
	public void setOldestDate(){
			SimpleDateFormat format1 = new SimpleDateFormat("MM-dd-yyyy");

			String formatted = format1.format(getOldest().getDate().getTime()); 
		 
			this.olddate = formatted;
	}
	
	public void setNewestDate(){
		SimpleDateFormat format1 = new SimpleDateFormat("MM-dd-yyyy");

		String formatted = format1.format(getNewest().getDate().getTime()); 
	 
		this.newdate = formatted;
	}
	
	@Override
	   public String toString() {
	      return "Album:" + albumName + "     Number of Photos:" + photoCount + " Oldest Photo:" + olddate + " Newest Photo:" + newdate;
	   }
}
