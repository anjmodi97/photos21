Some clarifications:
After editing a photo(caption,tag), to see the updates on an photo, the user must hit view photo again. The values then will be updated.
For search, the user must first add the tags and then click search.  

Some UI issues:
For rename album, the album name's value is updated but it does not show up in the list right away. The update shows up after a new album is added.
For caption near the thumbnail, the caption's value is updated but it does not show up in the list right away. The update shows up after a new photo is added.
For rename tag, the tag's value is updated but it does not show up in the list right away. The update shows up after a new tag is added.

Extra Credit/Added Functionality:
Clicking on the heart button allows the users to automatically move their favorited photos in a designated album. 

