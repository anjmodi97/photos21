/**written by Anjali Modi and Clara Le **/ 


package application;
	
import javafx.application.Application;

import javafx.fxml.FXMLLoader;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.Parent;

import javafx.scene.layout.GridPane;
//import javafx.scene.layout.BorderPane;

import java.io.IOException;

import controller.LoginController;
import controller.UserList;


public class Main extends Application  {
	
	
	Stage mstage; 
	
	public void start(Stage primaryStage) throws IOException, ClassNotFoundException {
		
		//UserList ulist2 = UserList.read();
		//System.out.println("in main app" + ulist2);
		
		mstage = primaryStage;
		mstage.setTitle("Photo Login");
		
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(getClass().getResource("/view/LogIn.fxml"));
			AnchorPane pane = (AnchorPane)loader.load();
			
			//LoginController controller = loader.getController();
			//controller.setMainStage(mstage);
					
			//BorderPane root = new BorderPane();
			Scene scene = new Scene(pane,400,400);
			// scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
			mstage.setScene(scene);
			mstage.show();
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		launch(args);
	}
}
