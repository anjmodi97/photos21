package model;

import java.io.Serializable;

public class Tag implements Serializable{
	private String tagField;
	private String tagValue;
	
	public Tag(String tf, String tv) {
		this.tagField = tf;
		this.tagValue = tv;
	}
	
	public void setTag(String tf, String tv) {
		this.tagField = tf;
		this.tagValue = tv;
	}
	
	public String getTagField() {
		return tagField;
	}
	
	public String getTagValue() {
		return tagValue;
	}
	
	@Override
	   public String toString() {
	      return "Name: " + getTagField() + " Value: " + getTagValue();
	   }
	@Override
	public boolean equals(Object o) {
	if(o == null || !(o instanceof Tag)){
		return false;
	}
	Tag temp = (Tag) o;
	if(temp.getTagField().equalsIgnoreCase(getTagField()) && temp.getTagValue().equalsIgnoreCase(getTagValue())) {
		return true;
	}
	
	return false;
}
}