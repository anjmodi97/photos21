package model;

import java.io.Serializable;
import java.util.*;

import javafx.scene.image.Image;

import java.text.*;
import java.time.LocalDate;
import java.time.ZoneId;

/**
 * Class for object photos, contains an array list of tags, a caption string, calendar date and a image that is serializable
 * @author Clara Le
 * @author Anjali Modi
 *
 */
public class Photo implements Serializable {
	
	private List<Tag> tags;
	private String caption;
	private Calendar date;
	private SImage photoImage;
	
	/**
	 * Constructor for photos
	 */
	public Photo() {
		photoImage = new SImage();
		caption = "";
		tags = new ArrayList<Tag>();
		date = Calendar.getInstance();
		date.set(Calendar.MILLISECOND, 0);
	}
	/**
	 * Constructor if user gives a photo
	 * @param pic = image given
	 */
	public Photo(Image pic) {
		this();
		photoImage.setImage(pic);
	}
	
	/**
	 * Method to set caption
	 * @param cap = caption to be set
	 */
	public void setCaption(String cap) {
		this.caption = cap;
	}
	/**
	 * method to get caption
	 * @return the String caption
	 */
	public String getCaption() {
		return caption;
	}
	
	/**
	 * method to add a new tag
	 * @param tagField = string tag field
	 * @param tagValue = string tag value
	 */
	public void addTag(String tagField, String tagValue) {
		tags.add(new Tag(tagField, tagValue));
	}
	
	/**
	 * method to remove a tag from list
	 * @param i = index of where the tag removed is
	 */
	public void removeTag(int i) {
		tags.remove(i);
	}
	
	public Image getImage() {
		return photoImage.getImage();
	}
	
	public SImage getSImage() {
		return photoImage;
	}
	
	/**
	 * method to edit a tag
	 * @param i = index of tag being edited
	 * @param newTagField = string tag field
	 * @param newTagValue = string tag value
	 */
	public void editTag(int i, String newTagField, String newTagValue) {
		tags.get(i).setTag(newTagField, newTagValue);
	}
	/**
	 * method to get the list of tags
	 * @return an array list of tags
	 */
	public List<Tag> getTags(){
		return tags;
	}
	/**
	 * method to get the date
	 * @return calendar date
	 */
	
	public boolean doesTagExist(Tag t) {
		for(Tag temp: tags) {
			if(t.equals(temp)) {
				return true;
			}
		}
		return false;
	}
	
	public Calendar getDate() {
		return date;
	}
	public boolean withinRange(LocalDate dateFrom, LocalDate dateUntil) {
		LocalDate currDate = date.getTime().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		if(currDate.isAfter(dateFrom) && currDate.isBefore(dateUntil)) {
			return true;
		}
		else if(currDate.isEqual(dateFrom) || currDate.isEqual(dateUntil)) {
			return true;
		}
		return false;
	}
	
	
}
