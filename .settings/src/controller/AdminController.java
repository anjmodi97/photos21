package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.fxml.FXMLLoader;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.Stage;

public class AdminController implements Logout {
	 
	@FXML
	private Button add;
	    
	@FXML
	 private Button delete;
	
	 @FXML
	 private TextField usertext;
	
	 @FXML
	 private ListView<User> view;

	
	 private ObservableList<User> observ;
	 private List<User> users = new ArrayList<User>();
	 private UserList ulist;
	 
	 public void start(Stage app_stage) throws ClassNotFoundException, IOException{
		// TODO Auto-generated method stub
		
		ulist = new UserList();
		
		if(ulist!=null) {
			ulist = UserList.read();
			users = ulist.getUserList();
			//System.out.println("list is not null" + ulist.toString());
		}
		else {
			System.out.println("list is still null in admin");
		}
		
		observ = FXCollections.observableArrayList(users);
		
		view.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<User>() {
		    @Override
		    public void changed(ObservableValue<? extends User> observable, User oldValue, User newValue) {
		        //System.out.println("ListView selection changed from oldValue = " 
		              //  + oldValue + " to newValue = " + newValue);
		    }
		});
		
		view.setItems(observ);
	}
	
	 @FXML 
		protected void handleLogoutButton(ActionEvent event) throws ClassNotFoundException {
		    	logout(event);       
		}
	 
	
	 public void onButton(ActionEvent e) throws ClassNotFoundException, IOException {
		
		view.setItems(observ);
		 
		if(e.getSource()== delete) {
			deleteuser();	
		}
		
		if(e.getSource()== add) {
			adduser();	
		}
			
		return;
	}
		
	 public void deleteuser() {
		
		 int index = view.getSelectionModel().getSelectedIndex();
		 
		User user = observ.get(index);
		
		if(user.getUsername().contains("admin")) {
			Alert err = new Alert(AlertType.ERROR);
			err.setContentText("Cannot delete admin");
			err.showAndWait();
			return;
		}
		
		observ.remove(user);
		users.remove(user);
		
			try {
				UserList.write(ulist);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
				
			view.setItems(observ);
		} 

	
	public void adduser() throws IOException {
		
		//System.out.println("user to be added" + usertext.getText());
		
		if(usertext.getText().equals("")==true) {
			Alert err = new Alert(AlertType.ERROR);
			err.setContentText("Empty username; Try again");
			err.showAndWait();
			return;
		}
		
		
		if(ulist.userExists(usertext.getText())) {
			Alert err = new Alert(AlertType.ERROR);
			err.setContentText("Username taken; Try again");
			err.showAndWait();
			return;
		}
		
		 User tempUser = new User(usertext.getText()); //store result
		 //System.out.println("temp:" + tempUser.getUsername());
		 
		 
		 
		observ.add(tempUser);
		//System.out.println(observ.get(0).getUsername());
		  users.add(tempUser);
		   //System.out.println("user added" + users.get(0).getUsername());
		   
		   
		   UserList.write(ulist);
			  
			   view.getSelectionModel().select(0);
		   
		  
	} 
	

}
