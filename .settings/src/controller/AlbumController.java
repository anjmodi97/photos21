package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.util.Callback;
import model.Album;
import controller.PhotoSubController;
/**
 * Class to control the AlbumSub window
 * @author Clara Le
 * @author Anajli Modi
 *
 */

public class AlbumController implements Logout{
	
	@FXML
	private Button addalbum;
	
	@FXML
	private Button rename;
	
	@FXML
	private Button delete;
	
	@FXML
	private Button exit;
	
	@FXML
	private Button search;
	
	@FXML
	private Button open;
	
	@FXML
	Text name;
	
	@FXML
	ListView<Album> albumview;
	
	 private ObservableList<Album> observ;
	 private List<Album> useralbum = new ArrayList<Album>();
	 private User curruser;
	 private UserList ulist;

	public void start(Stage app_stage)  {
		
		name.setText(curruser.getUsername()+ "'s albums");
		
		useralbum = curruser.getAlbums();
		
		//System.out.println(curruser.getUsername());
		
		
		//System.out.println("first album" + curruser.getAlbums().get(0));
		
		//if(useralbum==null) {
		//	System.out.println("nullllll");
		//}
		
		//System.out.println("first album" + useralbum.get(0).getAlbumName());
		
		observ = FXCollections.observableArrayList(useralbum);
		
		albumview.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Album>() {
		    @Override
		    public void changed(ObservableValue<? extends Album> observable, Album oldValue, Album newValue) {
		       // System.out.println("ListView selection changed from oldValue = " 
		                //+ oldValue + " to newValue = " + newValue);
		    }
		}); 
		
		albumview.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Album>() {
		    @Override
		    public void changed(ObservableValue<? extends Album> observable, Album oldValue, Album newValue) {
		       // System.out.println("ListView selection changed from oldValue = " 
		              //  + oldValue + " to newValue = " + newValue);
		    }
		});

		albumview.setItems(observ);
	}
	
	 @FXML 
		protected void handleLogoutButton(ActionEvent event) throws ClassNotFoundException {
		    	logout(event);       
		}
	 
	 public void onButton(ActionEvent e) throws ClassNotFoundException, IOException {
		 
			
			if(e.getSource()== addalbum) {
				addalbum();
				
			}
			if(e.getSource()== delete) {
				deletealbum();
				
			}
			if(e.getSource()== rename) {
				renamealbum();
				
			}
			if(e.getSource()== search) {
				search(e);
				
			}
			if(e.getSource()== open) {
				openAlbum(e);
				
			}
				
		}


	private void search(ActionEvent e) throws IOException{
		// TODO Auto-generated method stub
		FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/SearchSub.fxml"));
        Parent parent = (Parent) loader.load();
        SearchController controller = loader.<SearchController>getController();
     //  System.out.println("Loaded controller");
        controller.setUser(curruser);
        controller.setUserList(ulist);
        Scene scene = new Scene(parent);
        
        Stage app_stage = (Stage) ((Node) e.getSource()).getScene().getWindow();	
      //  System.out.println("Got source");
		controller.start(app_stage);
       //  System.out.println("Started scene");
		app_stage.setScene(scene);
		app_stage.show();
	}

	private void renamealbum() {
		
		int index = albumview.getSelectionModel().getSelectedIndex();
		
		Dialog<Album> dialog = new Dialog<>();
		dialog.setTitle("Rename Album");
		dialog.setResizable(true);
		   
		Label albumLabel = new Label("New Name: ");
		TextField albumTextField = new TextField();
		albumTextField.setPromptText("new album name");
		   
		GridPane grid = new GridPane();
		grid.add(albumLabel, 1, 1);
		grid.add(albumTextField, 2, 1);
		   
		dialog.getDialogPane().setContent(grid);
		   
		ButtonType buttonTypeOk = new ButtonType("Rename", ButtonData.OK_DONE);
		dialog.getDialogPane().getButtonTypes().add(buttonTypeOk);
		   
		/*dialog.setResultConverter(new Callback<ButtonType, Album>() {
			@Override
			public Album call(ButtonType b) {
				if (b == buttonTypeOk) {
											   
					return new Album(albumTextField.getText().trim());
				}
				return null;
			}

			
		});*/ 
		   
		Optional<Album> result = dialog.showAndWait();
		   
		if (result.isPresent()) {
			
			String str = albumTextField.getText();
			
			if(curruser.albumexists(str)==true) {
				Alert err = new Alert(AlertType.ERROR);
				err.setContentText("Album name taken; Try again");
				err.showAndWait();
				return;
			}
			
			useralbum.get(index).setAlbumName(albumTextField.getText());
			observ.get(index).setAlbumName(albumTextField.getText());
			albumview.setItems(observ);
			
			try {
				UserList.write(ulist);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
				   
		}
		
		albumview.setItems(observ);
		
	}

	private void deletealbum() {
	
		int index = albumview.getSelectionModel().getSelectedIndex();
		 
		Album alb = observ.get(index);
		
		
		observ.remove(alb);
		useralbum.remove(alb);
		albumview.setItems(observ);
		
			try {
				UserList.write(ulist);
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
				
	}

	private void addalbum() {
		
		
		Dialog<Album> dialog = new Dialog<>();
		dialog.setTitle("Create a New Album");
		dialog.setResizable(true);
		   
		Label albumLabel = new Label("Album Name: ");
		TextField albumTextField = new TextField();
		albumTextField.setPromptText("Album Name");
		   
		GridPane grid = new GridPane();
		grid.add(albumLabel, 1, 1);
		grid.add(albumTextField, 2, 1);
		   
		dialog.getDialogPane().setContent(grid);
		   
		ButtonType buttonTypeOk = new ButtonType("Add", ButtonData.OK_DONE);
		dialog.getDialogPane().getButtonTypes().add(buttonTypeOk);
		   
		dialog.setResultConverter(new Callback<ButtonType, Album>() {
			@Override
			public Album call(ButtonType b) {
				if (b == buttonTypeOk) {
											   
					return new Album(albumTextField.getText().trim());
				}
				return null;
			}

			
		});
		   
		Optional<Album> result = dialog.showAndWait();
		   
		if (result.isPresent()) {
			
			String str = albumTextField.getText();
			
			if(str.equals("")==true) {
				Alert err = new Alert(AlertType.ERROR);
				err.setContentText("No name entered; Try again");
				err.showAndWait();
				return;
			}
			
			if(curruser.albumexists(str)==true) {
				Alert err = new Alert(AlertType.ERROR);
				err.setContentText("Album name taken; Try again");
				err.showAndWait();
				return;
			}
			
			
			
			Album tempAlbum = (Album) result.get(); //store result
			observ.add(tempAlbum);
			useralbum.add(tempAlbum);
			albumview.setItems(observ);
			try {
				UserList.write(ulist);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
				   
		}
		
		
	}
	
	public void openAlbum(ActionEvent e) throws IOException {
		
		int index = albumview.getSelectionModel().getSelectedIndex();
		
		FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/PhotoSub.fxml"));
        Parent parent = (Parent) loader.load();
        PhotoSubController controller = loader.<PhotoSubController>getController();
        //send user index to album list controller
        controller.setAlbum(useralbum.get(index));
        //System.out.println(useralbum.get(index).getAlbumName());
        controller.setUser(curruser);
        controller.setUlist(ulist);
        Scene scene = new Scene(parent);
        
        Stage app_stage = (Stage) ((Node) e.getSource()).getScene().getWindow();	
        
		controller.start(app_stage);
         
		app_stage.setScene(scene);
		app_stage.show();
		
		
		
	}

	public void setUlist(UserList ulist) {
		// TODO Auto-generated method stub
		this.ulist = ulist;
		//System.out.println("in album"+ ulist.toString());
		
	}


	public void setUser(User user) {
		// TODO Auto-generated method stub
		this.curruser = user;
		//System.out.println("in album user is" + curruser.getUsername());
	}
}
