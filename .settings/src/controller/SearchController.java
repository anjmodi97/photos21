package controller;

import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

//import controller.PhotoSubController.PhotoCell;
import javafx.application.*;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.control.TableView;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import javafx.util.Callback;
import javafx.scene.control.ListView;
import model.*;
import javafx.util.Callback;
import javafx.scene.control.TextField;

public class SearchController implements Logout {
	
	@FXML
	private Button logout;
	
	@FXML
	private Button search;
	
	@FXML
	private Button addAlbum;
	
	@FXML
	private Button addTag;
	
	@FXML
	private Button deleteTag;
	
	@FXML
	private Button goBack;
	
	@FXML
	DatePicker dateFrom;
	
	@FXML
	DatePicker dateUntil;
	
	@FXML
	private TextField tagField;
	
	@FXML
	private TextField tagValue;
	
	@FXML
	ListView<Tag> tagView;
	
	@FXML
	ListView<Photo> photoView;
	
	
	private ObservableList<Photo> observPhoto;
	private ObservableList<Tag> observTag;
	private List<Photo> photoList;
	private List<Tag> tagList;
	private User currentUser;
	private UserList useList;
	
	public void start(Stage mainStage) {
		System.out.println("Got into start");
		tagList = new ArrayList<Tag>();
		observTag = FXCollections.observableArrayList(tagList);
		photoList = new ArrayList<Photo>();
		List<Album> userAlbums = currentUser.getAlbums();
		System.out.println("Got Albums");
		if(userAlbums != null) {
			
		int i;
		for(i = 0; i < userAlbums.size(); i++) {
			
			for(Photo tempPhoto: userAlbums.get(i).getPhotos()) {
				if(!photoList.contains(tempPhoto)) {
					photoList.add(tempPhoto);
				}
			}
		}
		}
		//System.out.println("getting photos");
		addAlbum.setDisable(photoList.isEmpty());
		
		//System.out.println("set disable");
		observPhoto = FXCollections.observableArrayList(photoList);
		//System.out.println("set observable list");
		
		photoView.setCellFactory(new Callback<ListView<Photo>, ListCell<Photo>>(){
			
			@Override
			public ListCell<Photo> call(ListView<Photo> p) {
				
				return new PhotoCell();
			}
		});	
		
		//System.out.println("made photo view");
		
		photoView.setItems(observPhoto);
		//System.out.println("set photo view");
		if(tagList!=null) {
			tagView.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Tag>() {
			    @Override
			    public void changed(ObservableValue<? extends Tag> observable, Tag oldValue, Tag newValue) {
			        System.out.println("ListView selection changed from oldValue = " 
			                + oldValue + " to newValue = " + newValue);
			    }
			});} 
		tagView.setItems(observTag);	
		
	}
	
	 private class PhotoCell extends ListCell<Photo> {
			
			AnchorPane apane = new AnchorPane();
			StackPane spane = new StackPane();
			
			ImageView imageView = new ImageView();
			Label captionLabel = new Label();
		
			public PhotoCell() {
				super();
			
				imageView.setFitWidth(45.0);
				imageView.setFitHeight(45.0);
				imageView.setPreserveRatio(true);
				
				StackPane.setAlignment(imageView, Pos.CENTER);
				
				spane.getChildren().add(imageView);
				
				spane.setPrefHeight(55.0);
				spane.setPrefWidth(45.0);
				
				AnchorPane.setLeftAnchor(spane, 0.0);
				
				AnchorPane.setLeftAnchor(captionLabel, 55.0);
				AnchorPane.setTopAnchor(captionLabel, 0.0);
			
				apane.getChildren().addAll(spane, captionLabel);
				
				apane.setPrefHeight(55.0);
				
				captionLabel.setMaxWidth(300.0);
				
				setGraphic(apane);
			}
			
			
			@Override
			public void updateItem(Photo photo, boolean empty) {
				super.updateItem(photo, empty);
				setText(null);
				if(photo == null)
				{
					imageView.setImage(null);
					captionLabel.setText("");
				
				}
				if (photo != null) {
					imageView.setImage(photo.getImage());
					captionLabel.setText("Caption: " + photo.getCaption());
				}
			}
			
		}
	
	@FXML
	protected void addTag() {
		Tag temp;
		String tf = tagField.getText();
		String tv = tagValue.getText();
		
		if(tf.isEmpty()) {
			Alert err = new Alert(AlertType.ERROR);
			err.setContentText("Error: Must input tag field!");
			err.showAndWait();
			return;
		}
		if(tv.isEmpty()) {
			Alert err = new Alert(AlertType.ERROR);
			err.setContentText("Error: Must input tag value!");
			err.showAndWait();
			return;
		}
		tagField.setText("");
		tagValue.setText("");
		for(Tag check: tagList) {
			if(check.getTagField().equals(tf) && check.getTagValue().equals(tv)) {
				Alert err = new Alert(AlertType.ERROR);
				err.setContentText("Error: Cannot have duplicate tag in list!");
				err.showAndWait();
				return;
			}
		}
		
		temp = new Tag(tf, tv);
		
		observTag.add(temp);
		tagList.add(temp);
	}
	
	@FXML
	private void deleteTag(ActionEvent event) {
		int index = tagView.getSelectionModel().getSelectedIndex();
		 
		Tag t = observTag.get(index);
		
		
		observTag.remove(t);
		tagList.remove(t);
		
			try {
				UserList.write(useList);
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
				
			tagView.setItems(observTag);
	}
	
	@FXML
	protected void searchPhotos(ActionEvent event) {
		List<Photo> searchList = new ArrayList<Photo>();
		List<Album> userAlbums = currentUser.getAlbums();
		int i;
		for(i = 0; i < userAlbums.size(); i++) {
			
			for(Photo tempPhoto: userAlbums.get(i).getPhotos()) {
				if(!searchList.contains(tempPhoto)) {
					searchList.add(tempPhoto);
				}
			}
		}
		LocalDate ldateTo;
		LocalDate ldateFrom;
		
		//must have some type of search parameter
		if(tagList.isEmpty() && dateFrom.getValue() == null && dateUntil.getValue() == null) {
			Alert err = new Alert(AlertType.ERROR);
			err.setContentText("Error: Must input at least 1 search parameter");
			err.showAndWait();
			return;
		}
		//must have dates in chronological order
		if(dateFrom.getValue() != null && dateUntil.getValue() != null) {
			if(dateFrom.getValue().isAfter(dateUntil.getValue())) {
				Alert err = new Alert(AlertType.ERROR);
				err.setContentText("Error: Search date must be in chronological order!");
				err.showAndWait();
				return;
			}
		}
		
		//if 0 - 1 date parameter inputted
		if(dateFrom.getValue() == null) {
			ldateFrom = LocalDate.MIN;
		}
		else {
			ldateFrom = dateFrom.getValue();
		}
		
		if(dateUntil.getValue() == null) {
			ldateTo = LocalDate.MAX;
		}
		else {
			ldateTo = dateUntil.getValue();
		}
		//clear and then readd with search results
		photoList.clear();
		observPhoto.clear();
		for(Photo t: searchList) {
			if(tagList.isEmpty()) { //only use date to search
				if(t.withinRange(ldateFrom, ldateTo)) {
					observPhoto.add(t);
					photoList.add(t);
				}
			}
			else if(t.withinRange(ldateFrom, ldateTo)) {
					for(Tag tmp: t.getTags()) {
						if(tagList.contains(tmp)) {
							observPhoto.add(t);
							photoList.add(t);
							break;
						}
					}
			}
		}
		addAlbum.setDisable(photoList.isEmpty());
	}
	
	@FXML
	protected void createAlbum(ActionEvent event) throws IOException {
		Dialog<Album> dialog = new Dialog<>();
		dialog.setTitle("Create a New Album");
		dialog.setResizable(true);
		   
		Label albumLabel = new Label("Album Name: ");
		TextField albumTextField = new TextField();
		albumTextField.setPromptText("Album Name");
		   
		GridPane grid = new GridPane();
		grid.add(albumLabel, 1, 1);
		grid.add(albumTextField, 2, 1);
		  
		dialog.getDialogPane().setContent(grid);
		   
		ButtonType buttonTypeOk = new ButtonType("Add", ButtonData.OK_DONE);
		dialog.getDialogPane().getButtonTypes().add(buttonTypeOk);
		   
		dialog.setResultConverter(new Callback<ButtonType, Album>() {
			@Override
			public Album call(ButtonType b) {
				if (b == buttonTypeOk) {
											   
					return new Album(albumTextField.getText().trim());
				}
				return null;
			}

			
		});
		Optional<Album> result = dialog.showAndWait();
		   
		if (result.isPresent()) {
			
			String str = albumTextField.getText();
			
			if(str.equals("")==true) {
				Alert err = new Alert(AlertType.ERROR);
				err.setContentText("No name entered; Try again");
				err.showAndWait();
				return;
			}
			
			if(currentUser.albumexists(str)==true) {
				Alert err = new Alert(AlertType.ERROR);
				err.setContentText("Album name taken; Try again");
				err.showAndWait();
				return;
			}
			
			
			Album tempAlbum = (Album) result.get(); //store result
			//observ.add(tempAlbum);
			for(Photo t: photoList) {
				tempAlbum.addPhoto(t);
			}
			currentUser.getAlbums().add(tempAlbum);
			try {
				UserList.write(useList);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		//	if (observ.size() >=1) {
			//	albumview.setItems(observ);
			//}
				   
		}
			
			
	
	
		
	}
	public void onButton(ActionEvent event) throws ClassNotFoundException, IOException{
		if(event.getSource()== goBack) {
			goBack(event);
			
		}
		if(event.getSource()== logout) {
			handleLogoutButton(event);
		}
		
	}
	
	private void handleLogoutButton(ActionEvent event) throws ClassNotFoundException {
    	logout(event);       
	}
	
	private void goBack(ActionEvent event) {
		Parent parent = null;
		
		FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/NonAdminSub.fxml"));
		try {
			parent = (Parent) loader.load();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		        
		AlbumController controller = loader.<AlbumController>getController();
		
		controller.setUlist(useList);
		controller.setUser(useList.getUserByUsername(currentUser.getUsername()));
		
		Scene scene = new Scene(parent);
					
		Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();	
	                
		controller.start(app_stage);
	             
	    app_stage.setScene(scene);
	    app_stage.show();  
		
	}
	
	public void setUser(User use) {
		this.currentUser = use;
	}
	
	public void setUserList(UserList ulist) {
		this.useList = ulist;
	}
}