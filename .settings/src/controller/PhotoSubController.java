package controller;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Optional;

import javax.imageio.ImageIO;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.event.EventType;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.util.Callback;
import model.Album;
import model.Photo;
import model.SImage;
import model.Tag;
import controller.User;
import controller.UserList;

public class PhotoSubController implements Logout{
	
	@FXML
	private Button add;
	
	@FXML
	private Button remove;
	
	@FXML
	private Button exit;
	
	@FXML
	private Button search;
	
	@FXML
	private Button goback;
	
	@FXML
	private Button addtag;
	
	@FXML
	private Button edittag;
	
	@FXML
	private Button deletetag;
	
	@FXML
	private Button view;
	
	@FXML
	private Button caption;
	
	@FXML
	private Button next;
	
	@FXML
	private Button prev;
	
	@FXML
	private Button move;
	
	@FXML
	private Button copy;
	
	@FXML
	private Button fav;
	
	
	@FXML
	Text aname;
	
	@FXML
	Text datetext;
	
	@FXML
	ImageView photoview;
	
	@FXML
	TextArea captiontext;
	
	@FXML
	ListView<Photo> pview;
	
	@FXML
	ListView<Tag> taglist;
	
	 private ObservableList<Photo> observ;
	 private ObservableList<Tag> observt;
	 private List<Photo> photos = new ArrayList<Photo>();
	 private List<Tag> tags = new ArrayList<Tag>();
	 private User curruser;
	 private Album album;
	 private UserList ulist;
	 private int pos;

	public void start(Stage app_stage) {
		// TODO Auto-generated method stub
	
		 aname.setText(album.getAlbumName() + " album");
		 
		 photos = album.getPhotos();
			
		observ = FXCollections.observableArrayList(photos);
		
		pview.setCellFactory(new Callback<ListView<Photo>, ListCell<Photo>>(){
			
			@Override
			public ListCell<Photo> call(ListView<Photo> p) {
				
				return new PhotoCell();
			}
		});	
		


		
		pview.setItems(observ);
	
		if(tags!=null) {
			taglist.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Tag>() {
			    @Override
			    public void changed(ObservableValue<? extends Tag> observable, Tag oldValue, Tag newValue) {
			       // System.out.println("ListView selection changed from oldValue = " 
			                //+ oldValue + " to newValue = " + newValue);
			    }
			});} 
		
		taglist.setItems(observt);
	}
	
	

	 public void onButton(ActionEvent e) throws ClassNotFoundException, IOException {
		 
			
			if(e.getSource()== add) {
				addPhoto(e);
				
			}
			if(e.getSource()== remove) {
				removePhoto();
				
			}
			if(e.getSource()== addtag) {
				addTag();
				
			}
			if(e.getSource()== edittag) {
				editTag();
				
			}
			if(e.getSource()== deletetag) {
				deleteTag();
				
			}
			
			if(e.getSource()== goback) {
				goBack(e);
				
			}
			if(e.getSource()== view) {
				pos = viewphoto();
				//System.out.println("pos" + pos);
				
			}
			if(e.getSource()== move) {
				movePhoto();
				
			}
			if(e.getSource()== copy) {
				copyPhoto();
				
			}
			if(e.getSource()== fav) {
				addtofav();
				
			}
			
			if(e.getSource()== next) {
				if(pos==photos.size()-1) {
					pos=photos.size()-1;
				}else {
					pos++;
					//System.out.println("pnew" + pos);
				}
				nextPhoto(pos);
				
			}
			
			if(e.getSource()== prev) {
				if(pos==0) {
					pos=0;
				}
				else {
					pos--;
				}
				prevPhoto(pos);
				
			}
				
		}
	 
	 
	 
	 
	



	private void prevPhoto(int index) {
		
		
		
		 captiontext.setText(observ.get(index).getCaption());
		 
		 SimpleDateFormat format1 = new SimpleDateFormat("MM-dd-yyyy");

		 String formatted = format1.format(observ.get(index).getDate().getTime());
		
		 	datetext.setText(formatted);
		
			tags = observ.get(index).getTags();
			observt = FXCollections.observableArrayList(tags);
			//tags.get(0).getTagField()
			taglist.setItems(observt);
			
			photoview.setImage(observ.get(index).getImage());
		
	}



	private void nextPhoto(int index) {
		
		System.out.println("index:" + index);
		
		captiontext.setText(photos.get(index).getCaption());
		 
		 SimpleDateFormat format1 = new SimpleDateFormat("MM-dd-yyyy");

		 String formatted = format1.format(observ.get(index).getDate().getTime());
		
		 	datetext.setText(formatted);
		
			tags = observ.get(index).getTags();
			observt = FXCollections.observableArrayList(tags);
			//tags.get(0).getTagField()
			taglist.setItems(observt);
			
			photoview.setImage(observ.get(index).getImage());
		
	}

	private class PhotoCell extends ListCell<Photo> {
			
			AnchorPane apane = new AnchorPane();
			StackPane spane = new StackPane();
			
			ImageView imageView = new ImageView();
			Label captionLabel = new Label();
		
			public PhotoCell() {
				super();
			
				imageView.setFitWidth(45.0);
				imageView.setFitHeight(45.0);
				imageView.setPreserveRatio(true);
				
				StackPane.setAlignment(imageView, Pos.CENTER);
				
				spane.getChildren().add(imageView);
				
				spane.setPrefHeight(55.0);
				spane.setPrefWidth(45.0);
				
				AnchorPane.setLeftAnchor(spane, 0.0);
				
				AnchorPane.setLeftAnchor(captionLabel, 55.0);
				AnchorPane.setTopAnchor(captionLabel, 0.0);
			
				apane.getChildren().addAll(spane, captionLabel);
				
				apane.setPrefHeight(55.0);
				
				captionLabel.setMaxWidth(300.0);
				
				setGraphic(apane);
			}
			
			
			@Override
			public void updateItem(Photo photo, boolean empty) {
				super.updateItem(photo, empty);
				setText(null);
				if(photo == null)
				{
					imageView.setImage(null);
					captionLabel.setText("");
				
				}
				if (photo != null) {
					imageView.setImage(photo.getImage());
					captionLabel.setText("Caption: " + photo.getCaption());
					
					caption.setOnAction(new EventHandler<ActionEvent>() {
						@Override public void handle(ActionEvent e) {
							addCaption(e, photo);
						}
					});
				}
			}
			
		}
	 
	 
		private void copyPhoto() {
			int index = pview.getSelectionModel().getSelectedIndex();
			
			Dialog<Album> dialog = new Dialog<>();
			dialog.setTitle("Copy Photo");
			dialog.setResizable(true);
			
			Label copyphoto = new Label("Copy to Another Album:");
			
			List<String> albumtext = new ArrayList<String>();
			
			for(Album albumn: curruser.getAlbums()) {
				
				String tempalbum = albumn.getAlbumName();
				if(albumn.getAlbumName()!=album.getAlbumName()) {
					albumtext.add(tempalbum);
				}
				
			}
			
			ObservableList<String> albumlist;
			albumlist = FXCollections.observableArrayList(albumtext);
			
			ComboBox<String> options = new ComboBox<String>(albumlist);
			
			GridPane grid = new GridPane();
			grid.add(copyphoto, 1, 1);
			grid.add(options, 1, 2);
			   
			dialog.getDialogPane().setContent(grid);
			   
			ButtonType buttonTypeOk = new ButtonType("Done", ButtonData.OK_DONE);
			dialog.getDialogPane().getButtonTypes().add(buttonTypeOk);
			   
			Optional<Album> result = dialog.showAndWait();
			
			if (result.isPresent()) {
				
				String newalbumtext = options.getSelectionModel().getSelectedItem();
				Album newalbum = curruser.getThisAlbum(newalbumtext);
				newalbum.addPhoto(observ.get(index));
				
				try {
					UserList.write(ulist);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
			
			
	}

		private void movePhoto() {
			int index = pview.getSelectionModel().getSelectedIndex();
			
			Dialog<Album> dialog = new Dialog<>();
			dialog.setTitle("Move Photo");
			dialog.setResizable(true);
			
			Label movephoto = new Label("Move to Album:");
			
			List<String> albumtext = new ArrayList<String>();
			
			for(Album albumn: curruser.getAlbums()) {
				
				String tempalbum = albumn.getAlbumName();
				if(albumn.getAlbumName()!=album.getAlbumName()) {
					albumtext.add(tempalbum);
				}
				
			}
			
			ObservableList<String> albumlist;
			albumlist = FXCollections.observableArrayList(albumtext);
			
			ComboBox<String> options = new ComboBox<String>(albumlist);
			
			GridPane grid = new GridPane();
			grid.add(movephoto, 1, 1);
			grid.add(options, 1, 2);
			   
			dialog.getDialogPane().setContent(grid);
			   
			ButtonType buttonTypeOk = new ButtonType("Done", ButtonData.OK_DONE);
			dialog.getDialogPane().getButtonTypes().add(buttonTypeOk);
			   
			Optional<Album> result = dialog.showAndWait();
			
			if (result.isPresent()) {
				String newalbumtext = options.getSelectionModel().getSelectedItem();
				Album newalbum = curruser.getThisAlbum(newalbumtext);
				newalbum.addPhoto(observ.get(index));
				
				Photo p = observ.get(index);

				observ.remove(p);
				photos.remove(p);
				
				album.deletePhoto(p);
			
				try {
					UserList.write(ulist);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
			
		
	}

		private void addTag() {
			int index = pview.getSelectionModel().getSelectedIndex();
			
			Dialog<Album> dialog = new Dialog<>();
			dialog.setTitle("Add Tag");
			dialog.setResizable(true);
			   
			Label caption = new Label("Tag Name: ");
			TextField nameTextField = new TextField();
			nameTextField.setPromptText("name");
			Label recaption = new Label("Tag Value: ");
			TextField valueTextField = new TextField();
			valueTextField.setPromptText("value");
			   
			GridPane grid = new GridPane();
			grid.add(caption, 1, 1);
			grid.add(recaption, 1, 3);
			grid.add(nameTextField, 2, 1);
			grid.add(valueTextField, 2, 3);
			   
			dialog.getDialogPane().setContent(grid);
			   
			ButtonType buttonTypeOk = new ButtonType("Done", ButtonData.OK_DONE);
			dialog.getDialogPane().getButtonTypes().add(buttonTypeOk);
			   
			Optional<Album> result = dialog.showAndWait();
			   
			if (result.isPresent()) {
				
				Tag temp = new Tag(nameTextField.getText(), valueTextField.getText());
				
				if(observ.get(index).doesTagExist(temp)==true) {
					Alert err = new Alert(AlertType.ERROR);
					err.setContentText("duplicate tag; try again");
					err.showAndWait();
					return;
				}

				
				tags = observ.get(index).getTags();
				
				tags.add(temp);
				observt = FXCollections.observableArrayList(tags);
				observt.add(temp);
			
				
				try {
					UserList.write(ulist);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
			
		}
	
	private void deleteTag() {
		// TODO Auto-generated method stub
		int index = taglist.getSelectionModel().getSelectedIndex();
		 
		Tag t = observt.get(index);
		
		
		observt.remove(t);
		tags.remove(t);
		
			try {
				UserList.write(ulist);
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
				
			taglist.setItems(observt);
		
	}

	private void editTag() {
		int index = pview.getSelectionModel().getSelectedIndex();
		
		int indext = taglist.getSelectionModel().getSelectedIndex();
		
		Dialog<Album> dialog = new Dialog<>();
		dialog.setTitle("Edit Tag");
		dialog.setResizable(true);
		   
		Label caption = new Label("Tag Name: ");
		TextField nameTextField = new TextField();
		nameTextField.setPromptText("new name");
		Label recaption = new Label("Tag Value: ");
		TextField valueTextField = new TextField();
		valueTextField.setPromptText("value");
		   
		GridPane grid = new GridPane();
		grid.add(caption, 1, 1);
		grid.add(recaption, 1, 3);
		grid.add(nameTextField, 2, 1);
		grid.add(valueTextField, 2, 3);
		   
		dialog.getDialogPane().setContent(grid);
		   
		ButtonType buttonTypeOk = new ButtonType("Done", ButtonData.OK_DONE);
		dialog.getDialogPane().getButtonTypes().add(buttonTypeOk);
		   
		Optional<Album> result = dialog.showAndWait();
		   
		if (result.isPresent()) {
			
			//Tag temp = new Tag(nameTextField.getText(), valueTextField.getText());
			
			tags = observ.get(index).getTags();
			
			System.out.println(tags.get(indext).getTagField() + " and " + tags.get(indext).getTagValue());
			
			tags.get(indext).setTag(nameTextField.getText(), valueTextField.getText());
			observt = FXCollections.observableArrayList(tags);
			taglist.setItems(observt);
		
			
			try {
				UserList.write(ulist);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		
	}
		

	private void addPhoto(ActionEvent event) throws IOException {
		FileChooser fileChooser = new FileChooser();
		
		FileChooser.ExtensionFilter extFilterJPG = new FileChooser.ExtensionFilter("JPG files (*.jpg)", "*.JPG");
        FileChooser.ExtensionFilter extFilterPNG = new FileChooser.ExtensionFilter("PNG files (*.png)", "*.PNG");
        fileChooser.getExtensionFilters().addAll(extFilterJPG, extFilterPNG);
		
		fileChooser.setTitle("Upload Photo");
		Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
		File file = fileChooser.showOpenDialog(app_stage);
		
		if (file == null)
			return;
		
        BufferedImage bufferedImage = ImageIO.read(file);
        Image image = SwingFXUtils.toFXImage(bufferedImage, null);
        
        //check to see if this photo exists in the album
        SImage tempImage = new SImage();
        tempImage.setImage(image);
        for (Photo p: album.getPhotos()) {
        	if (tempImage.equals(p.getSImage())) {
        		return;
        	}
        }
        
        Photo tempPhoto = null;
        boolean photoFound = false;
        
        //check to see if this photo exists in other albums
        //then tempPhoto = the photo object that it equals to
        for (Album a: curruser.getAlbums()) {
        	for (Photo p: a.getPhotos()) {
        		if (tempImage.equals(p.getSImage())) {
        			tempPhoto = p;
        			photoFound = true;
        			System.out.println("Found the photo!");
        			break;
        		}
        		if (photoFound)
        			break;
        	}
        }
        
        //else, create a new photo object
        if (!photoFound)
        	tempPhoto = new Photo(image);
        
        
        album.addPhoto(tempPhoto);
        observ.add(tempPhoto);
        
        //updateItem(tempPhoto);
        
        //pview.setItems(observ);
        
		UserList.write(ulist);
		
	}
	
	 private void addtofav() throws IOException {
			// TODO Auto-generated method stub
			 int index = pview.getSelectionModel().getSelectedIndex();
			 
			 System.out.println("index here:" + index);
			 Album fav;
					 
			 if(curruser.albumexists("favorite")) {
				 fav = curruser.getThisAlbum("favorite");
			 }
			 else {
				 fav = new Album("favorite");
				 curruser.addAlbum(fav);
			 }
			 
		        Image image = photos.get(index).getImage();
		        
		        //check to see if this photo exists in the album
		        SImage tempImage = new SImage();
		        tempImage.setImage(image);
		        for (Photo p: fav.getPhotos()) {
		        	if (tempImage.equals(p.getSImage())) {
		        		return;
		        	}
		        }
		        
		        Photo tempPhoto = null;
		        boolean photoFound = false;
		        
		        //check to see if this photo exists in other albums
		        //then tempPhoto = the photo object that it equals to
		        for (Album a: curruser.getAlbums()) {
		        	for (Photo p: a.getPhotos()) {
		        		if (tempImage.equals(p.getSImage())) {
		        			tempPhoto = p;
		        			photoFound = true;
		        			System.out.println("Found the photo!");
		        			break;
		        		}
		        		if (photoFound)
		        			break;
		        	}
		        }
		        
		        //else, create a new photo object
		        if (!photoFound)
		        	tempPhoto = new Photo(image);
		        
		        fav.addPhoto(tempPhoto);
		   
				UserList.write(ulist);
		        
			
		}
	

	private void removePhoto() {
		
		//add alert to confirm deletion of photo 
		
		int index = pview.getSelectionModel().getSelectedIndex();
		 
		Photo p = observ.get(index);
		
		
		observ.remove(p);
		photos.remove(p);
		album.deletePhoto(p);
		
			try {
				UserList.write(ulist);
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
				
			pview.setItems(observ);
		
	}

	public void addCaption(ActionEvent e, Photo photo) {
		
		//int index = pview.getSelectionModel().getSelectedIndex();
		
		Dialog<Album> dialog = new Dialog<>();
		dialog.setTitle("Caption");
		dialog.setResizable(true);
		   
		Label caption = new Label("Caption: ");
		TextField captionTextField = new TextField();
		captionTextField.setPromptText("caption");
		Label recaption = new Label("Re-Caption: ");
		TextField recaptionTextField = new TextField();
		recaptionTextField.setPromptText("new caption");
		   
		GridPane grid = new GridPane();
		grid.add(caption, 1, 1);
		grid.add(recaption, 1, 3);
		grid.add(captionTextField, 2, 1);
		grid.add(recaptionTextField, 2, 3);
		   
		dialog.getDialogPane().setContent(grid);
		   
		ButtonType buttonTypeOk = new ButtonType("Done", ButtonData.OK_DONE);
		dialog.getDialogPane().getButtonTypes().add(buttonTypeOk);
		   
		Optional<Album> result = dialog.showAndWait();
		   
		if (result.isPresent() && photo.getCaption().equals("")) {
			
			photo.setCaption(captionTextField.getText().trim());
			photo.setCaption(captionTextField.getText().trim());
			
			pview.setItems(observ);
			
			
			try {
				UserList.write(ulist);
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
		}
		
		//add alerts to make sure that fields are not null
		
		else {
			photo.setCaption(recaptionTextField.getText().trim());
			photo.setCaption(recaptionTextField.getText().trim());
			
			pview.setItems(observ);
			
			try {
				UserList.write(ulist);
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
			
		
			
				   
		
	}

	private void goBack(ActionEvent event) {
		Parent parent = null;
		
		FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/NonAdminSub.fxml"));
		try {
			parent = (Parent) loader.load();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		        
		AlbumController controller = loader.<AlbumController>getController();
		
		controller.setUlist(ulist);
		controller.setUser(ulist.getUserByUsername(curruser.getUsername()));
		
		Scene scene = new Scene(parent);
					
		Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();	
	                
		controller.start(app_stage);
	             
	    app_stage.setScene(scene);
	    app_stage.show();  
		
	}

	private int viewphoto() {
		// TODO Auto-generated method stub
		
		int index = pview.getSelectionModel().getSelectedIndex();
		
		 captiontext.setText(observ.get(index).getCaption());
		 
		 SimpleDateFormat format1 = new SimpleDateFormat("MM-dd-yyyy");

		 String formatted = format1.format(observ.get(index).getDate().getTime());
		
		 	datetext.setText(formatted);
		
			tags = observ.get(index).getTags();
			observt = FXCollections.observableArrayList(tags);
			//tags.get(0).getTagField()
			taglist.setItems(observt);
			
			photoview.setImage(observ.get(index).getImage());
			
			return index;
	}

	@FXML 
	protected void handleLogoutButton(ActionEvent event) throws ClassNotFoundException {
    	logout(event);       
	}

	public void setUlist(UserList ulist) {
		// TODO Auto-generated method stub
		this.ulist = ulist;
		
	}

	public void setAlbum(Album a) {
		// TODO Auto-generated method stub
		this.album = a;
	}

	public void setUser(User curruser) {
		// TODO Auto-generated method stub
		this.curruser = curruser;
	}
	
	
}
